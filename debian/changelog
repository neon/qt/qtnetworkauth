qtnetworkauth-everywhere-src (5.15.16-0neon) noble; urgency=medium

  * new release.

 -- KDE neon <neon@kde.org>  Mon, 21 Jul 2024 07:59:20 +1000

qtnetworkauth-everywhere-src (5.15.13-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.13.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 13 Mar 2024 19:58:01 +0300

qtnetworkauth-everywhere-src (5.15.12-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.12.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 24 Dec 2023 20:04:17 +0300

qtnetworkauth-everywhere-src (5.15.10-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 08 Jul 2023 19:15:28 +0300

qtnetworkauth-everywhere-src (5.15.10-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.10.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 12 Jun 2023 00:12:46 +0300

qtnetworkauth-everywhere-src (5.15.9-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.9.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 23 Apr 2023 21:00:27 +0300

qtnetworkauth-everywhere-src (5.15.8-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 13 Jan 2023 12:01:54 +0400

qtnetworkauth-everywhere-src (5.15.8-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.8.
  * Bump Standards-Version to 4.6.2, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 07 Jan 2023 17:53:21 +0400

qtnetworkauth-everywhere-src (5.15.7-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 17 Dec 2022 18:20:20 +0300

qtnetworkauth-everywhere-src (5.15.7-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.7.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 04 Dec 2022 20:53:07 +0300

qtnetworkauth-everywhere-src (5.15.6-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 29 Sep 2022 11:55:48 +0300

qtnetworkauth-everywhere-src (5.15.6-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.6.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 13 Sep 2022 13:31:52 +0300

qtnetworkauth-everywhere-src (5.15.5-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.5.
  * Use symver directive to catch all private symbols at once.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 30 Jul 2022 21:15:46 +0300

qtnetworkauth-everywhere-src (5.15.4-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 13 Jun 2022 21:36:35 +0300

qtnetworkauth-everywhere-src (5.15.4-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.4.
  * Update symbols files from buildds’ logs.
  * Bump Standards-Version to 4.6.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 14 May 2022 12:56:33 +0300

qtnetworkauth-everywhere-src (5.15.3-1) experimental; urgency=medium

  * New upstream release.
  * Update debian/watch.
  * Bump Qt build-dependencies to 5.15.3.
  * Update email address for Patrick Franz.
  * Bump Standards-Version to 4.6.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 06 Mar 2022 18:58:08 +0300

qtnetworkauth-everywhere-src (5.15.2-2) unstable; urgency=medium

  * Add support for nodoc build profile.
  * Build-depend only on the needed documentation tools, not on the large
    qttools5-dev-tools package.
  * Bump Standards-Version to 4.5.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 11 Dec 2020 11:32:02 +0300

qtnetworkauth-everywhere-src (5.15.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.2.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 22 Nov 2020 21:28:37 +0300

qtnetworkauth-everywhere-src (5.15.1-2) unstable; urgency=medium

  * Update debian/libqt5networkauth5.symbols from buildds’ logs.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 28 Oct 2020 21:53:35 +0300

qtnetworkauth-everywhere-src (5.15.1-1) experimental; urgency=medium

  [ Scarlett Moore ]
  * New upstream release.
  * Bump Qt build-dependencies to 5.15.1.
  * Bump debhelper-compat to 13.
  * Refresh symbols -
    Removed missing optional symbol.

  [ Dmitry Shachnev ]
  * Drop dh_missing override, not needed with debhelper compat 13.
  * Use ${DEB_HOST_MULTIARCH} substitution in the install files.
  * Refresh disable_tests_network.diff.
  * Make sure private headers are not installed.
  * Remove build path from libQt5NetworkAuth.prl for reproducibility.
  * Remove -Wl,--as-needed from linker flags, it is now default.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 19 Sep 2020 21:03:02 +0300

qtnetworkauth-everywhere-src (5.14.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 24 Jun 2020 12:29:44 +0300

qtnetworkauth-everywhere-src (5.14.2-1) experimental; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: Let dh_auto_configure use a host qmake. (Closes: #943407)

  [ Patrick Franz ]
  * New upstream release.
  * Bump Standards-Version to 4.5.0, no changes needed.
  * Bump debhelper-compatibility to 12:
    - Switch debhelper build dependency to debhelper-compat 12.
    - Remove debian/compat.
  * Update the Uploaders-field.
  * Bump Qt build-dependencies to 5.14.2.
  * Add Rules-Requires-Root field.
  * Update debian/copyright.
  * Update debian/libqt5networkauth5.symbols from the current build log.

 -- Patrick Franz <patfra71@gmail.com>  Wed, 29 Apr 2020 19:47:30 +0200

qtnetworkauth-everywhere-src (5.12.5-1) unstable; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.5.
  * Update debian/libqt5networkauth5.symbols from buildds’ logs.
  * Bump Standards-Version to 4.4.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 21 Oct 2019 18:56:26 +0300

qtnetworkauth-everywhere-src (5.12.4-1) experimental; urgency=medium

  [ Scarlett Moore ]
  * Update doc-base to be consistent with other modules.
  * Update my email.

  [ Dmitry Shachnev ]
  * New upstream release.
  * Drop explicit qtchooser build-dependency, qtbase5-dev depends on it.
  * Bump Qt build-dependencies to 5.12.4.
  * Simplify debian/rules by using debian/not-installed file.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 02 Jul 2019 15:31:51 +0300

qtnetworkauth-everywhere-src (5.12.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.2.
  * Refresh debian/patches/disable_tests_network.diff.
  * Update debian/libqt5networkauth5.symbols from the current build log.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 26 Mar 2019 14:49:58 +0300

qtnetworkauth-everywhere-src (5.11.3-2) unstable; urgency=medium

  * Team upload.
  * Remove the spurious +dfsg from the version of the qttools5-dev-tools build
    dependency.
  * Bump Standards-Version to 4.3.0, no changes required.
  * Add Build-Depends-Package to the symbols file.

 -- Pino Toscano <pino@debian.org>  Thu, 27 Dec 2018 11:37:52 +0100

qtnetworkauth-everywhere-src (5.11.3-1) unstable; urgency=medium

  [ Pino Toscano ]
  * Do not ship the private qt_lib_networkauth_private.pri qmake config.

  [ Simon Quigley ]
  * Change my email to tsimonq2@debian.org now that I am a Debian Developer.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Update symbols files with buildds' logs.
  * New upstream release.
    - Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 26 Dec 2018 19:59:29 -0300

qtnetworkauth-everywhere-src (5.11.2-1) unstable; urgency=medium

  [ Jonathan Riddell ]
  * Initial package

  [ Scarlett Moore ]
  * Initial release (Closes: #883412)

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 02 Nov 2018 17:52:19 +0300
